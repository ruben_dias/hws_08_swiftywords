//
//  AppDelegate.swift
//  Project08
//
//  Created by Ruben Dias on 15/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
